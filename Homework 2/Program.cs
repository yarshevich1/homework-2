﻿while (true)
{
    Console.WriteLine("Enter your name");
    var name = Console.ReadLine();

    if (name?.ToLower() == "q")
        break;

    if (!string.IsNullOrEmpty(name))
        WriteToConsole($"Hello {name}", ConsoleColor.Cyan);

    else WriteToConsole("You didn't enter a name", ConsoleColor.Red);
}

static void WriteToConsole(string text, ConsoleColor color)
{
    Console.ForegroundColor = color;
    Console.WriteLine(text);
    Console.ForegroundColor = ConsoleColor.White;
}
